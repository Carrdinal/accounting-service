package com.carrdinal.accountingservice.it;

import com.carrdinal.accountingservice.dto.AccountDto;
import com.carrdinal.accountingservice.dto.BigDecimalDto;
import com.carrdinal.accountingservice.dto.TxDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.lessThan;
import static org.hamcrest.Matchers.lessThanOrEqualTo;
import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.POST;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class ConcurrentIntegrationTests {

    @LocalServerPort
    private int port;

    private TestRestTemplate restTemplate = new TestRestTemplate();

    private HttpHeaders headers = new HttpHeaders();

    @Test
    void concurrentTopUpAndGetBalanceTest() throws Exception {
        Instant start = Instant.now();
        Integer annie = createAccountAndGetId("Annie", BigDecimal.ZERO);

        int threadCount = 100;
        CountDownLatch latch = new CountDownLatch(1);
        Collection<Future<BigDecimal>> futures = new ArrayList<>(threadCount);
        ExecutorService service = Executors.newFixedThreadPool(100);

        for (int t = 0; t < threadCount; ++t) {
            futures.add(
                    service.submit(
                            () -> {
                                latch.await();
                                return topUpAccountAndReturnBalance(annie, BigDecimal.ONE);
                            }
                    )
            );
        }
        latch.countDown();
        Set<BigDecimal> balances = new HashSet<>();
        for (Future<BigDecimal> f : futures) {
            balances.add(f.get());
        }

        assertThat(getAccountBalance(annie), equalTo(new BigDecimal("100")));
        assertThat(balances, hasSize(threadCount));

        Instant finish = Instant.now();
        long timeElapsed = Duration.between(start, finish).toMillis();
        System.out.println(String.format("Took %d ms", timeElapsed));
    }

    @Test
    void concurrentPaymentsBetweenTwoAccounts() throws Exception {
        Instant start = Instant.now();

        Integer annie = createAccountAndGetId("Annie", new BigDecimal(100));
        Integer beth = createAccountAndGetId("Beth", new BigDecimal(100));

        int annieThreadCount = 100;
        int bethThreadCount = 50;
        CountDownLatch latch = new CountDownLatch(1);
        Collection<Future<Boolean>> annieSuccessfulPaymentFutures = new ArrayList<>(annieThreadCount);
        Collection<Future<Boolean>> bethSuccessfulPaymentFutures = new ArrayList<>(bethThreadCount);
        ExecutorService service = Executors.newFixedThreadPool(100);

        for (int t = 0; t < annieThreadCount; ++t) {
            annieSuccessfulPaymentFutures.add(
                    service.submit(
                            () -> {
                                latch.await();
                                return transferFunds(annie, beth, BigDecimal.ONE);
                            }
                    )
            );
        }

        for (int t = 0; t < bethThreadCount; ++t) {
            bethSuccessfulPaymentFutures.add(
                    service.submit(
                            () -> {
                                latch.await();
                                return transferFunds(beth, annie, BigDecimal.ONE);
                            }
                    )
            );
        }

        latch.countDown();

        // Check all the payments were successful since both have enough funds
        int annieSuccessfulPayments = 0;
        for (Future<Boolean> f : annieSuccessfulPaymentFutures) {
            annieSuccessfulPayments += f.get() ? 1 : 0;
        }
        assertThat(annieSuccessfulPayments, equalTo(annieThreadCount));

        int bethSuccessfulPayments = 0;
        for (Future<Boolean> f : bethSuccessfulPaymentFutures) {
            bethSuccessfulPayments += f.get() ? 1 : 0;
        }
        assertThat(bethSuccessfulPayments, equalTo(bethThreadCount));

        assertThat(getAccountBalance(annie), equalTo(new BigDecimal("50")));
        assertThat(getAccountBalance(beth), equalTo(new BigDecimal("150")));


        Instant finish = Instant.now();
        long timeElapsed = Duration.between(start, finish).toMillis();
        System.out.println(String.format("Took %d ms", timeElapsed));
    }

    @Test
    void concurrentOverspendTranferNotAllowedTest() throws Exception {
        Instant start = Instant.now();

        Integer annie = createAccountAndGetId("Annie", BigDecimal.ZERO);
        Integer beth = createAccountAndGetId("Beth", new BigDecimal(100));

        int annieThreadCount = 100;
        int bethThreadCount = 50;
        CountDownLatch latch = new CountDownLatch(1);
        Collection<Future<Boolean>> annieSuccessfulPaymentFutures = new ArrayList<>(annieThreadCount);
        Collection<Future<Boolean>> bethSuccessfulPaymentFutures = new ArrayList<>(bethThreadCount);
        ExecutorService service = Executors.newFixedThreadPool(100);

        for (int t = 0; t < annieThreadCount; ++t) {
            annieSuccessfulPaymentFutures.add(
                    service.submit(
                            () -> {
                                latch.await();
                                return transferFunds(annie, beth, BigDecimal.ONE);
                            }
                    )
            );
        }

        for (int t = 0; t < bethThreadCount; ++t) {
            bethSuccessfulPaymentFutures.add(
                    service.submit(
                            () -> {
                                latch.await();
                                return transferFunds(beth, annie, BigDecimal.ONE);
                            }
                    )
            );
        }

        latch.countDown();

        // Annie should at most be able to send as much money as Beth sends to her
        int annieSuccessfulPayments = 0;
        for (Future<Boolean> f : annieSuccessfulPaymentFutures) {
            annieSuccessfulPayments += f.get().equals(Boolean.TRUE) ? 1 : 0;
        }
        assertThat(annieSuccessfulPayments, lessThan(bethThreadCount));

        int bethSuccessfulPayments = 0;
        for (Future<Boolean> f : bethSuccessfulPaymentFutures) {
            bethSuccessfulPayments += f.get() ? 1 : 0;
        }
        assertThat(bethSuccessfulPayments, equalTo(bethThreadCount));

        assertThat(getAccountBalance(annie), lessThanOrEqualTo(new BigDecimal("50")));
        assertThat(getAccountBalance(beth), greaterThanOrEqualTo(new BigDecimal("50")));

        Instant finish = Instant.now();
        long timeElapsed = Duration.between(start, finish).toMillis();
        System.out.println(String.format("Took %d ms", timeElapsed));
    }

    @Test
    void concurrentWithdrawOverspendNotAllowedTest() throws Exception {
        Instant start = Instant.now();

        Integer annie = createAccountAndGetId("Annie", new BigDecimal(50));

        int annieThreadCount = 100;
        CountDownLatch latch = new CountDownLatch(1);
        Collection<Future<Boolean>> annieSuccessfulPaymentFutures = new ArrayList<>(annieThreadCount);
        ExecutorService service = Executors.newFixedThreadPool(100);

        for (int t = 0; t < annieThreadCount; ++t) {
            annieSuccessfulPaymentFutures.add(
                    service.submit(
                            () -> {
                                latch.await();
                                return withdrawFunds(annie, BigDecimal.ONE);
                            }
                    )
            );
        }

        latch.countDown();

        // Half the withdrawals should pass, the other half should fail due to insufficient funds
        int annieSuccessfulPayments = 0;
        for (Future<Boolean> f : annieSuccessfulPaymentFutures) {
            annieSuccessfulPayments += f.get().equals(Boolean.TRUE) ? 1 : 0;
        }
        assertThat(annieSuccessfulPayments, equalTo(50));

        assertThat(getAccountBalance(annie), equalTo(BigDecimal.ZERO));

        Instant finish = Instant.now();
        long timeElapsed = Duration.between(start, finish).toMillis();
        System.out.println(String.format("Took %d ms", timeElapsed));
    }

    private Boolean withdrawFunds(Integer accountId, BigDecimal amount) {
        HttpEntity<BigDecimalDto> entity = new HttpEntity<>(new BigDecimalDto(amount), headers);

        ResponseEntity<Object> response = restTemplate.exchange(
                createURLWithPort(String.format("/accounts/%d/withdraw", accountId)), POST, entity, Object.class
        );

        return response.getStatusCodeValue() == 200;
    }


    private Boolean transferFunds(Integer senderId, Integer recipientId, BigDecimal amount) {
        TxDto txDto = TxDto.builder()
                .sender(AccountDto.builder().id(senderId).build())
                .recipient(AccountDto.builder().id(recipientId).build())
                .amount(amount)
                .build();

        HttpEntity<TxDto> entity = new HttpEntity<>(txDto, headers);

        ResponseEntity<Object> response = restTemplate.exchange(
                createURLWithPort("/transactions"), POST, entity, Object.class
        );

        return response.getStatusCodeValue() == 200;
    }

    private BigDecimal getAccountBalance(Integer accountId) {
        HttpEntity<Integer> entity = new HttpEntity<>(null, headers);

        ResponseEntity<BigDecimal> response = restTemplate.exchange(
                createURLWithPort(String.format("/accounts/%d/balance", accountId)), GET, entity, BigDecimal.class
        );

        return response.getBody();
    }

    private BigDecimal topUpAccountAndReturnBalance(Integer accountId, BigDecimal amount) {
        HttpEntity<BigDecimalDto> entity = new HttpEntity<>(new BigDecimalDto(amount), headers);

        ResponseEntity<BigDecimal> response = restTemplate.exchange(
                createURLWithPort(String.format("/accounts/%d/top-up", accountId)), POST, entity, BigDecimal.class
        );

        return response.getBody();
    }

    private Integer createAccountAndGetId(String name, BigDecimal balance) {
        AccountDto accountDto = AccountDto.builder().name(name).balance(balance).build();
        HttpEntity<AccountDto> entity = new HttpEntity<>(accountDto, headers);

        ResponseEntity<Integer> response = restTemplate.exchange(
                createURLWithPort("/accounts"), POST, entity, Integer.class
        );

        return response.getBody();
    }

    private String createURLWithPort(String uri) {
        return String.format("http://localhost:%d/api/v1%s", port, uri);
    }

}
