package com.carrdinal.accountingservice.service;

import com.carrdinal.accountingservice.model.Account;
import com.carrdinal.accountingservice.repository.AccountsRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock.ReadLock;
import java.util.concurrent.locks.ReentrantReadWriteLock.WriteLock;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

class AccountServiceTest {

    @Mock
    private AccountsRepository mockAccountsRepository;

    @Spy
    private ReentrantReadWriteLock spyLock;
    private ReadLock mockReadLock;
    private WriteLock mockWriteLock;

    private AccountService accountServiceUnderTest;

    @BeforeEach
    void setUp() {
        initMocks(this);
        mockWriteLock = spy(spyLock.writeLock());
        mockReadLock = spy(spyLock.readLock());
        when(spyLock.writeLock()).thenReturn(mockWriteLock);
        when(spyLock.readLock()).thenReturn(mockReadLock);
        accountServiceUnderTest = new AccountService(mockAccountsRepository, spyLock);
    }

    @Test
    void testCreateAccount() {
        // Setup
        final Account inputAccount = Account.builder().name("Annie").balance(BigDecimal.ZERO).build();
        final Account savedAccount = Account.builder().id(1).name("Annie").balance(BigDecimal.ZERO).build();

        when(mockAccountsRepository.save(inputAccount)).thenReturn(savedAccount);

        // Run the test
        final Integer result = accountServiceUnderTest.createAccount(inputAccount);

        // Verify the results
        verify(mockAccountsRepository, times(1)).save(inputAccount);

        verifyNoMoreInteractions(mockAccountsRepository);

        verify(mockWriteLock, times(1)).lock();
        verify(mockWriteLock, times(1)).unlock();

        assertEquals(Integer.valueOf(1), result);
    }

    @Test
    void testDeleteAccount() {
        // Setup
        final Integer accountId = 1;

        // Run the test
        accountServiceUnderTest.deleteAccount(accountId);

        // Verify the results
        verify(mockAccountsRepository, times(1)).deleteById(accountId);

        verifyNoMoreInteractions(mockAccountsRepository);

        verify(mockWriteLock, times(1)).lock();
        verify(mockWriteLock, times(1)).unlock();
    }

    @Test
    void testGetAccount() {
        // Setup
        final Integer accountId = 1;
        final Account savedAccount = Account.builder().id(1).name("Annie").balance(BigDecimal.ZERO).build();
        when(mockAccountsRepository.findById(accountId)).thenReturn(Optional.of(savedAccount));

        // Run the test
        final Account result = accountServiceUnderTest.getAccount(accountId);

        // Verify the results
        verify(mockAccountsRepository, times(1)).findById(accountId);

        verifyNoMoreInteractions(mockAccountsRepository);

        verify(mockReadLock, times(1)).lock();
        verify(mockReadLock, times(1)).unlock();

        assertEquals(savedAccount, result);
    }

    @Test
    void testGetAccount_ThrowsNotFound_ifAccountNotFound() {
        // Setup
        final Integer accountId = 1;
        when(mockAccountsRepository.findById(accountId)).thenReturn(Optional.empty());

        // Run the test
        ResponseStatusException exception = assertThrows(ResponseStatusException.class, () -> accountServiceUnderTest.getAccount(accountId));

        // Verify the results
        assertThat(exception.getStatus(), equalTo(HttpStatus.NOT_FOUND));

        verify(mockAccountsRepository, times(1)).findById(accountId);

        verifyNoMoreInteractions(mockAccountsRepository);

        verify(mockReadLock, times(1)).lock();
        verify(mockReadLock, times(1)).unlock();
    }

    @Test
    void testGetAccounts() {
        // Setup
        final List<Account> savedAccounts = Arrays.asList(
                Account.builder().id(1).name("Annie").balance(BigDecimal.ZERO).build(),
                Account.builder().id(2).name("Beth").balance(BigDecimal.ZERO).build()
        );

        when(mockAccountsRepository.findAll()).thenReturn(savedAccounts);

        // Run the test
        final List<Account> result = accountServiceUnderTest.getAccounts();

        // Verify the results
        verify(mockAccountsRepository, times(1)).findAll();

        verifyNoMoreInteractions(mockAccountsRepository);

        verify(mockReadLock, times(1)).lock();
        verify(mockReadLock, times(1)).unlock();

        assertEquals(savedAccounts, result);
    }

    @Test
    void testGetAccountsEmptyList() {
        // Setup
        final List<Account> savedAccounts = Collections.emptyList();
        when(mockAccountsRepository.findAll()).thenReturn(savedAccounts);

        // Run the test
        final List<Account> result = accountServiceUnderTest.getAccounts();

        // Verify the results
        assertEquals(savedAccounts, result);
    }

    @Test
    void testGetAccountBalance() {
        // Setup
        final Integer accountId = 1;
        final Account savedAccount = Account.builder().id(1).name("Annie").balance(new BigDecimal(20)).build();
        when(mockAccountsRepository.findById(accountId)).thenReturn(Optional.of(savedAccount));


        // Run the test
        final BigDecimal result = accountServiceUnderTest.getAccountBalance(accountId);

        // Verify the results
        verify(mockAccountsRepository, times(1)).findById(accountId);

        verifyNoMoreInteractions(mockAccountsRepository);

        verify(mockReadLock, times(1)).lock();
        verify(mockReadLock, times(1)).unlock();

        assertEquals(savedAccount.getBalance(), result);
    }

    @Test
    void testTopUp() {
        // Setup
        final Integer accountId = 1;
        final Account savedAccount = Account.builder().id(1).name("Annie").balance(new BigDecimal(20)).build();
        final Account savedAccountAfterTopUp = Account.builder().id(1).name("Annie").balance(new BigDecimal(25)).build();
        final BigDecimal amount = new BigDecimal(5);
        final BigDecimal expectedResult = savedAccountAfterTopUp.getBalance();

        when(mockAccountsRepository.findById(accountId)).thenReturn(Optional.of(savedAccount));
        when(mockAccountsRepository.save(any())).thenReturn(savedAccountAfterTopUp);

        // Run the test
        final BigDecimal result = accountServiceUnderTest.topUp(accountId, amount);

        // Verify the results
        verify(mockAccountsRepository, times(1)).findById(accountId);
        verify(mockAccountsRepository, times(1)).save(savedAccountAfterTopUp);

        verifyNoMoreInteractions(mockAccountsRepository);

        verify(mockWriteLock, times(1)).lock();
        verify(mockWriteLock, times(1)).unlock();

        assertEquals(expectedResult, result);
    }

    @Test
    void testWithdraw() {
        // Setup
        final Integer accountId = 1;
        final Account savedAccount = Account.builder().id(1).name("Annie").balance(new BigDecimal(20)).build();
        final Account savedAccountAfterTopUp = Account.builder().id(1).name("Annie").balance(new BigDecimal(15)).build();
        final BigDecimal amount = new BigDecimal(5);
        final BigDecimal expectedResult = savedAccountAfterTopUp.getBalance();

        when(mockAccountsRepository.findById(accountId)).thenReturn(Optional.of(savedAccount));
        when(mockAccountsRepository.save(any())).thenReturn(savedAccountAfterTopUp);

        // Run the test
        final BigDecimal result = accountServiceUnderTest.withdraw(accountId, amount);

        // Verify the results
        verify(mockAccountsRepository, times(1)).findById(accountId);
        verify(mockAccountsRepository, times(1)).save(savedAccountAfterTopUp);

        verifyNoMoreInteractions(mockAccountsRepository);

        verify(mockWriteLock, times(1)).lock();
        verify(mockWriteLock, times(1)).unlock();

        assertEquals(expectedResult, result);
    }

    @Test
    void testWithdraw_ThrowsBadRequest_ifAccountDoesNotHaveRequiredBalance() {
        // Setup
        final Integer accountId = 1;
        final Account savedAccount = Account.builder().id(1).name("Annie").balance(new BigDecimal(0)).build();
        final BigDecimal amount = new BigDecimal(5);

        when(mockAccountsRepository.findById(accountId)).thenReturn(Optional.of(savedAccount));


        // Run the test
        ResponseStatusException exception = assertThrows(ResponseStatusException.class, () -> accountServiceUnderTest.withdraw(accountId, amount));

        // Verify the results
        assertThat(exception.getStatus(), equalTo(HttpStatus.BAD_REQUEST));

        verify(mockAccountsRepository, times(1)).findById(accountId);

        verifyNoMoreInteractions(mockAccountsRepository);

        verify(mockWriteLock, times(1)).lock();
        verify(mockWriteLock, times(1)).unlock();

    }
}
