package com.carrdinal.accountingservice.service;

import com.carrdinal.accountingservice.model.Account;
import com.carrdinal.accountingservice.model.Tx;
import com.carrdinal.accountingservice.repository.AccountsRepository;
import com.carrdinal.accountingservice.repository.TransactionRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.server.ResponseStatusException;

import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.springframework.http.HttpStatus.BAD_REQUEST;

class TransactionServiceTest {

    @Mock
    private TransactionRepository mockTransactionRepository;

    @Mock
    private AccountsRepository mockAccountsRepository;

    @Spy
    private ReentrantReadWriteLock spyLock;
    private ReentrantReadWriteLock.ReadLock mockReadLock;
    private ReentrantReadWriteLock.WriteLock mockWriteLock;

    private TransactionService transactionServiceUnderTest;

    @BeforeEach
    void setUp() {
        initMocks(this);
        mockWriteLock = spy(spyLock.writeLock());
        mockReadLock = spy(spyLock.readLock());
        when(spyLock.writeLock()).thenReturn(mockWriteLock);
        when(spyLock.readLock()).thenReturn(mockReadLock);
        transactionServiceUnderTest = new TransactionService(mockTransactionRepository, mockAccountsRepository, spyLock);
    }

    @Test
    void testCreateTransaction() {
        // Setup
        final Integer expectedResult = 1;
        final Account expectedSender = Account.builder().id(1).name("Annie").balance(new BigDecimal(15)).build();
        final Account expectedRecipient = Account.builder().id(2).name("Beth").balance(new BigDecimal(5)).build();

        final Account sender = Account.builder().id(1).name("Annie").balance(new BigDecimal(20)).build();
        final Account recipient = Account.builder().id(2).name("Beth").balance(new BigDecimal(0)).build();

        final Tx inputTx = Tx.builder().sender(sender).recipient(recipient).amount(new BigDecimal(5)).build();
        final Tx savedTx = Tx.builder().id(expectedResult).sender(sender).recipient(recipient).amount(new BigDecimal(10)).build();

        when(mockTransactionRepository.save(inputTx)).thenReturn(savedTx);
        when(mockAccountsRepository.findById(1)).thenReturn(Optional.of(sender));
        when(mockAccountsRepository.findById(2)).thenReturn(Optional.of(recipient));

        // Run the test
        final Integer result = transactionServiceUnderTest.createTransaction(inputTx);

        // Verify the results
        verify(mockAccountsRepository, times(1)).findById(sender.getId());
        verify(mockAccountsRepository, times(1)).findById(recipient.getId());
        verify(mockAccountsRepository, times(1)).save(expectedSender);
        verify(mockAccountsRepository, times(1)).save(expectedRecipient);
        verify(mockTransactionRepository, times(1)).save(inputTx);

        verifyNoMoreInteractions(mockAccountsRepository);
        verifyNoMoreInteractions(mockTransactionRepository);

        verify(mockWriteLock, times(1)).lock();
        verify(mockWriteLock, times(1)).unlock();

        assertEquals(expectedResult, result);
    }

    @Test
    void testCreateTransaction_ThrowsBadRequest_ifSenderAccountDoesntExist() {
        // Setup
        final Account sender = Account.builder().id(1).name("Annie").balance(new BigDecimal(20)).build();
        final Account recipient = Account.builder().id(2).name("Beth").balance(new BigDecimal(0)).build();

        final Tx inputTx = Tx.builder().sender(sender).recipient(recipient).amount(new BigDecimal(5)).build();

        when(mockAccountsRepository.findById(sender.getId())).thenReturn(Optional.empty());

        // Run the test
        ResponseStatusException exception = assertThrows(ResponseStatusException.class, () -> transactionServiceUnderTest.createTransaction(inputTx));

        assertThat(exception.getStatus(), equalTo(BAD_REQUEST));

        verify(mockAccountsRepository, times(1)).findById(sender.getId());

        verifyNoMoreInteractions(mockAccountsRepository);
        verifyNoMoreInteractions(mockTransactionRepository);

        verify(mockWriteLock, times(1)).lock();
        verify(mockWriteLock, times(1)).unlock();
    }

    @Test
    void testCreateTransaction_ThrowsBadRequest_ifRecipientAccountDoesntExist() {
        // Setup
        final Account sender = Account.builder().id(1).name("Annie").balance(new BigDecimal(20)).build();
        final Account recipient = Account.builder().id(2).name("Beth").balance(new BigDecimal(0)).build();

        final Tx inputTx = Tx.builder().sender(sender).recipient(recipient).amount(new BigDecimal(5)).build();

        when(mockAccountsRepository.findById(sender.getId())).thenReturn(Optional.of(sender));
        when(mockAccountsRepository.findById(recipient.getId())).thenReturn(Optional.empty());

        // Run the test
        ResponseStatusException exception = assertThrows(ResponseStatusException.class, () -> transactionServiceUnderTest.createTransaction(inputTx));

        assertThat(exception.getStatus(), equalTo(BAD_REQUEST));

        verify(mockAccountsRepository, times(1)).findById(sender.getId());
        verify(mockAccountsRepository, times(1)).findById(recipient.getId());

        verifyNoMoreInteractions(mockAccountsRepository);
        verifyNoMoreInteractions(mockTransactionRepository);

        verify(mockWriteLock, times(1)).lock();
        verify(mockWriteLock, times(1)).unlock();
    }

    @Test
    void testCreateTransaction_ThrowsBadRequest_ifSenderFundsTooLow() {
        // Setup
        final Account sender = Account.builder().id(1).name("Annie").balance(new BigDecimal(20)).build();
        final Account recipient = Account.builder().id(2).name("Beth").balance(new BigDecimal(0)).build();

        final Tx inputTx = Tx.builder().sender(sender).recipient(recipient).amount(new BigDecimal(25)).build();

        when(mockAccountsRepository.findById(sender.getId())).thenReturn(Optional.of(sender));

        // Run the test
        ResponseStatusException exception = assertThrows(ResponseStatusException.class, () -> transactionServiceUnderTest.createTransaction(inputTx));

        assertThat(exception.getStatus(), equalTo(BAD_REQUEST));

        verify(mockAccountsRepository, times(1)).findById(sender.getId());

        verifyNoMoreInteractions(mockAccountsRepository);
        verifyNoMoreInteractions(mockTransactionRepository);

        verify(mockWriteLock, times(1)).lock();
        verify(mockWriteLock, times(1)).unlock();
    }

    @Test
    void testGetTransactionForAccount() {
        // Setup
        final Integer transactionId = 1;

        final Account sender = Account.builder().id(1).name("Annie").balance(new BigDecimal(20)).build();
        final Account recipient = Account.builder().id(2).name("Beth").balance(new BigDecimal(0)).build();

        final Tx savedTx = Tx.builder().id(transactionId).sender(sender).recipient(recipient).amount(BigDecimal.ONE).build();

        when(mockTransactionRepository.findBySenderIdAndId(sender.getId(), savedTx.getId())).thenReturn(savedTx);

        // Run the test
        final Tx result = transactionServiceUnderTest.getTransactionForAccount(sender.getId(), transactionId);

        // Verify the results
        verify(mockTransactionRepository, times(1)).findBySenderIdAndId(sender.getId(), savedTx.getId());

        verifyNoMoreInteractions(mockAccountsRepository);
        verifyNoMoreInteractions(mockTransactionRepository);

        verify(mockReadLock, times(1)).lock();
        verify(mockReadLock, times(1)).unlock();

        assertEquals(savedTx, result);
    }

    @Test
    void testGetTransactionsForAccount() {
        // Setup
        final Account sender = Account.builder().id(1).name("Annie").balance(new BigDecimal(20)).build();
        final Account recipient = Account.builder().id(2).name("Beth").balance(new BigDecimal(0)).build();

        final Tx savedTx1 = Tx.builder().id(1).sender(sender).recipient(recipient).amount(BigDecimal.ONE).build();
        final Tx savedTx2 = Tx.builder().id(2).sender(sender).recipient(recipient).amount(BigDecimal.TEN).build();

        when(mockTransactionRepository.findAllBySenderId(sender.getId())).thenReturn(Arrays.asList(savedTx1, savedTx2));

        // Run the test
        final List<Tx> result = transactionServiceUnderTest.getTransactionsForAccount(sender.getId());

        // Verify the results
        verify(mockTransactionRepository, times(1)).findAllBySenderId(sender.getId());

        verifyNoMoreInteractions(mockAccountsRepository);
        verifyNoMoreInteractions(mockTransactionRepository);

        verify(mockReadLock, times(1)).lock();
        verify(mockReadLock, times(1)).unlock();

        assertEquals(Arrays.asList(savedTx1, savedTx2), result);
    }

    @Test
    void testGetTransactionsBetweenTwoAccounts() {
        // Setup
        final Integer transactionId = 1;

        final Account sender = Account.builder().id(1).name("Annie").balance(new BigDecimal(20)).build();
        final Account recipient = Account.builder().id(2).name("Beth").balance(new BigDecimal(0)).build();

        final Tx savedTx1 = Tx.builder().id(1).sender(sender).recipient(recipient).amount(BigDecimal.ONE).build();
        final Tx savedTx2 = Tx.builder().id(2).sender(sender).recipient(recipient).amount(BigDecimal.TEN).build();

        when(mockTransactionRepository.findAllBySenderIdAndRecipientId(sender.getId(), recipient.getId())).thenReturn(Arrays.asList(savedTx1, savedTx2));

        // Run the test
        final List<Tx> result = transactionServiceUnderTest.getTransactionsBetweenTwoAccounts(sender.getId(), recipient.getId());

        // Verify the results
        verify(mockTransactionRepository, times(1)).findAllBySenderIdAndRecipientId(sender.getId(), recipient.getId());

        verifyNoMoreInteractions(mockAccountsRepository);
        verifyNoMoreInteractions(mockTransactionRepository);

        verify(mockReadLock, times(1)).lock();
        verify(mockReadLock, times(1)).unlock();

        assertEquals(Arrays.asList(savedTx1, savedTx2), result);
    }
}
