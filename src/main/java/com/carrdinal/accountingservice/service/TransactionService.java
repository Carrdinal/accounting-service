package com.carrdinal.accountingservice.service;

import com.carrdinal.accountingservice.model.Account;
import com.carrdinal.accountingservice.model.Tx;
import com.carrdinal.accountingservice.repository.AccountsRepository;
import com.carrdinal.accountingservice.repository.TransactionRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

@Service
public class TransactionService {

    private final ReentrantReadWriteLock lock;
    private final TransactionRepository transactionRepository;
    private final AccountsRepository accountsRepository;

    public TransactionService(TransactionRepository transactionRepository, AccountsRepository accountsRepository, ReentrantReadWriteLock lock) {
        this.transactionRepository = transactionRepository;
        this.accountsRepository = accountsRepository;
        this.lock = lock;
    }

    public Integer createTransaction(Tx tx) {
        lock.writeLock().lock();
        try {
            // Get the sender account
            Account sender = accountsRepository.findById(tx.getSender().getId())
                    .orElseThrow(() -> new ResponseStatusException(BAD_REQUEST, "Sender account doesn't exist"));

            // Check the sender has required funds
            if (sender.getBalance().compareTo(tx.getAmount()) < 0) {
                throw new ResponseStatusException(BAD_REQUEST,
                        "Not enough funds in account to make transaction.");
            }

            // Get the recipients account
            Account recipient = accountsRepository.findById(tx.getRecipient().getId())
                    .orElseThrow(() -> new ResponseStatusException(BAD_REQUEST, "Recipient account doesn't exist"));

            sender.subtractFunds(tx.getAmount());
            recipient.addFunds(tx.getAmount());

            // Persist changes
            tx.setSender(accountsRepository.save(sender));
            tx.setRecipient(accountsRepository.save(recipient));
            return transactionRepository.save(tx).getId();

        } finally {
            lock.writeLock().unlock();
        }
    }

    public Tx getTransactionForAccount(Integer accountId, Integer transactionId) {
        lock.readLock().lock();
        try {
            return transactionRepository.findBySenderIdAndId(accountId, transactionId);
        } finally {
            lock.readLock().unlock();
        }
    }

    public List<Tx> getTransactionsForAccount(Integer accountId) {
        lock.readLock().lock();
        try {
            return transactionRepository.findAllBySenderId(accountId);
        } finally {
            lock.readLock().unlock();
        }
    }

    public List<Tx> getTransactionsBetweenTwoAccounts(Integer accountId, Integer withAccountId) {
        lock.readLock().lock();
        try {
            return transactionRepository.findAllBySenderIdAndRecipientId(accountId, withAccountId);
        } finally {
            lock.readLock().unlock();
        }
    }
}
