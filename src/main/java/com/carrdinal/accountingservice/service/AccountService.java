package com.carrdinal.accountingservice.service;

import com.carrdinal.accountingservice.model.Account;
import com.carrdinal.accountingservice.repository.AccountsRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@Service
public class AccountService {

    private final ReentrantReadWriteLock lock;

    private final AccountsRepository accountsRepository;

    public AccountService(AccountsRepository accountsRepository, ReentrantReadWriteLock lock) {
        this.accountsRepository = accountsRepository;
        this.lock = lock;
    }

    public synchronized Integer createAccount(Account account) {
        lock.writeLock().lock();
        try {
            return accountsRepository.save(account).getId();
        } finally {
            lock.writeLock().unlock();
        }
    }

    public void deleteAccount(Integer accountId) {
        lock.writeLock().lock();
        try {
            accountsRepository.deleteById(accountId);
        } finally {
            lock.writeLock().unlock();
        }
    }

    public Account getAccount(Integer accountId) {
        lock.readLock().lock();
        try {
            return accountsRepository.findById(accountId)
                    .orElseThrow(() -> new ResponseStatusException(NOT_FOUND, "Account not found"));
        } finally {
            lock.readLock().unlock();
        }
    }

    public List<Account> getAccounts() {
        lock.readLock().lock();
        try {
            return accountsRepository.findAll();
        } finally {
            lock.readLock().unlock();
        }
    }

    public BigDecimal getAccountBalance(Integer accountId) {
        return getAccount(accountId).getBalance();
    }

    public BigDecimal topUp(Integer accountId, BigDecimal amount) {
        lock.writeLock().lock();
        try {
            Account account = getAccountOrThrowNotFound(accountId);
            account.setBalance(account.getBalance().add(amount));
            return accountsRepository.save(account).getBalance();
        } finally {
            lock.writeLock().unlock();
        }
    }

    public BigDecimal withdraw(Integer accountId, BigDecimal amount) {
        lock.writeLock().lock();
        try {
            Account account = getAccountOrThrowNotFound(accountId);

            // Check the account has required funds to withdraw
            if (account.getBalance().compareTo(amount) < 0) {
                throw new ResponseStatusException(BAD_REQUEST,
                        "Not enough funds in account to make withdrawal.");
            }

            account.setBalance(account.getBalance().subtract(amount));
            return accountsRepository.save(account).getBalance();
        } finally {
            lock.writeLock().unlock();
        }
    }

    private Account getAccountOrThrowNotFound(Integer accountId) {
        return accountsRepository.findById(accountId)
                        .orElseThrow(() -> new ResponseStatusException(BAD_REQUEST, "Account doesn't exist"));
    }

}
