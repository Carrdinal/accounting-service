package com.carrdinal.accountingservice.mapper;

import com.carrdinal.accountingservice.dto.TxDto;
import com.carrdinal.accountingservice.model.Tx;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(uses = AccountMapper.class)
public interface TxMapper {

    TxMapper INSTANCE = Mappers.getMapper(TxMapper.class);

    Tx fromDto(TxDto dto);

    TxDto toDto(Tx tx);
}
