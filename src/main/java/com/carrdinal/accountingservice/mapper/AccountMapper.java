package com.carrdinal.accountingservice.mapper;

import com.carrdinal.accountingservice.dto.AccountDto;
import com.carrdinal.accountingservice.model.Account;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface AccountMapper {

    AccountMapper INSTANCE = Mappers.getMapper(AccountMapper.class);

    Account fromDto(AccountDto dto);

    AccountDto toDto(Account account);
}
