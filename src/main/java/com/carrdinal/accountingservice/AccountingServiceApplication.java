package com.carrdinal.accountingservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.concurrent.locks.ReentrantReadWriteLock;

@SpringBootApplication
public class AccountingServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(AccountingServiceApplication.class, args);
    }

    @Bean
    public ReentrantReadWriteLock accountLock(){
        return new ReentrantReadWriteLock();
    }

}
