package com.carrdinal.accountingservice.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TxDto {

    private Integer id;

    private AccountDto sender;

    private AccountDto recipient;

    private BigDecimal amount;

}
