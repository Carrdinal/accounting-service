package com.carrdinal.accountingservice.repository;

import com.carrdinal.accountingservice.model.Tx;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransactionRepository extends JpaRepository<Tx, Integer> {

    Tx findBySenderIdAndId(Integer senderId, Integer txId);

    List<Tx> findAllBySenderId(Integer senderId);

    List<Tx> findAllBySenderIdAndRecipientId(Integer senderId, Integer recipientId);

}
