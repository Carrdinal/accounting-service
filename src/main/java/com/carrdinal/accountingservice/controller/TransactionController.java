package com.carrdinal.accountingservice.controller;

import com.carrdinal.accountingservice.dto.TxDto;
import com.carrdinal.accountingservice.mapper.TxMapper;
import com.carrdinal.accountingservice.model.Tx;
import com.carrdinal.accountingservice.service.TransactionService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static java.util.stream.Collectors.toList;

@RestController
@RequestMapping("/api/v1")
public class TransactionController {

    private final TransactionService transactionService;

    public TransactionController(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    @PostMapping("/transactions")
    public Integer createTransaction(@RequestBody TxDto txDto) {
        Tx tx = TxMapper.INSTANCE.fromDto(txDto);
        return transactionService.createTransaction(tx);
    }

    @GetMapping("accounts/{accountId}/transactions/{transactionId}")
    public TxDto getTransactionForAccount(@PathVariable("accountId") Integer accountId,
                                          @PathVariable("transactionId") Integer transactionId) {
        return TxMapper.INSTANCE.toDto(transactionService.getTransactionForAccount(accountId, transactionId));
    }

    @GetMapping("accounts/{accountId}/transactions}")
    public List<TxDto> getTransactionsForAccount(@PathVariable("accountId") Integer accountId) {
        return transactionService.getTransactionsForAccount(accountId).stream()
                .map(TxMapper.INSTANCE::toDto)
                .collect(toList());
    }

    @GetMapping("accounts/{accountId1}/transactions/with/{accountId2}}")
    public List<TxDto> getTransactionsBetweenTwoAccounts(
            @PathVariable("accountId1") Integer accountId,
            @PathVariable("accountId2") Integer withAccountId) {
        return transactionService.getTransactionsBetweenTwoAccounts(accountId, withAccountId).stream()
                .map(TxMapper.INSTANCE::toDto)
                .collect(toList());
    }

}
