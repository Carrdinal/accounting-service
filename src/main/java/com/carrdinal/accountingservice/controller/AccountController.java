package com.carrdinal.accountingservice.controller;


import com.carrdinal.accountingservice.dto.AccountDto;
import com.carrdinal.accountingservice.dto.BigDecimalDto;
import com.carrdinal.accountingservice.mapper.AccountMapper;
import com.carrdinal.accountingservice.service.AccountService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1/accounts")
public class AccountController {

    private final AccountService accountService;

    private int count = 0;

    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @PostMapping
    public Integer createAccount(@RequestBody AccountDto accountDto) {
        return accountService.createAccount(AccountMapper.INSTANCE.fromDto(accountDto));
    }

    @DeleteMapping("{accountId}")
    public void deleteAccount(@PathVariable("accountId") Integer accountId) {
        accountService.deleteAccount(accountId);
    }

    @GetMapping("{accountId}")
    public AccountDto getAccount(@PathVariable("accountId") Integer accountId) {
        return AccountMapper.INSTANCE.toDto(accountService.getAccount(accountId));
    }

    @GetMapping
    public List<AccountDto> getAccounts() {
        return accountService.getAccounts().stream()
                .map(AccountMapper.INSTANCE::toDto)
                .collect(Collectors.toList());
    }

    @GetMapping("{accountId}/balance")
    public BigDecimal getAccountBalance(@PathVariable("accountId") Integer accountId) {
        return accountService.getAccountBalance(accountId);
    }

    @PostMapping("{accountId}/top-up")
    public BigDecimal topUp(@PathVariable("accountId") Integer accountId,
                            @RequestBody BigDecimalDto amount) {
        return accountService.topUp(accountId, amount.getValue());
    }

    @PostMapping("{accountId}/withdraw")
    public BigDecimal withdraw(@PathVariable("accountId") Integer accountId,
                               @RequestBody BigDecimalDto amount) {
        return accountService.withdraw(accountId, amount.getValue());
    }



}
