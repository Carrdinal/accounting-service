create table account (
  id int auto_increment,
  name varchar(20),
  balance decimal not null,
  primary key (id)
);

create table transaction (
  id int auto_increment,
  sender_id int not null,
  recipient_id int not null,
  amount decimal not null,
  foreign key fk_sender_id(sender_id) references account (id),
  foreign key fk_recipient_id(recipient_id) references account (id),
  primary key (id)
);