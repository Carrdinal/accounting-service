# Accounting Service

## API Endpoints

### Accounts

`POST /api/v1/accounts`

`GET /api/v1/accounts`

`GET /api/v1/accounts/{accountId}`

`GET /api/v1/accounts/{accountId}/balance`

`POST /api/v1/accounts/{accountId}/top-up`

`POST /api/v1/accounts/{accountId}/withdraw`

### Transactions

`POST /api/v1/transactions`

`GET /api/v1/accounts/{accountId}/transactions/{tranasctionId}`

`GET /api/v1/accounts/{accountId}/transactions`

`GET /api/v1/accounts/{accountId1}/transactions/with/{accountId2}`


## To build and run test suite:

```
cd docker && docker-compose up -d && cd ..
mvn verify
```

## To run the application

```
cd docker && docker-compose up -d && cd ..
mvn spring-boot:run
```

> **_NOTE:_** If using docker toolbox for windows first run `docker-machine env` to get the virtual-machine's host IP and then 
run `export DOCKER_HOST <host-ip>` before running the commands above. The application.property files will default to 
localhost which is fine for Docker for Windows 10 or macOS/Linux operating systems. This way Spring will be able to 
connect to the dockerized mySQL instance.
